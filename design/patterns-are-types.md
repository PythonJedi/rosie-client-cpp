# Patterns are Types

The basic idea stems from a thought in the RPL 1.0 syntax definition 
(`.../doc/rpl.md` Section "Matches/Captures")

>  The rationale for this decision [capturing every named pattern] is that the 
>  pattern name has the role of a type, and therefore can be used to tag a 
>  capture in a meaningful way. Another way to think about it is: if something 
>  is important enough that you want to see it captured in the output, then it 
>  deserves a name.

Asserting equivalence between named patterns in a pattern language and the 
concept of a type is a nice thing to say, but is it anything more than 
empty philosophizing to justify a design decision? It most certainly is. C++ 
isn't exactly known for an amazing type system, but it has enough to create a 
useful injection from RPL patterns to C++ types. Why make such a thing? It's 
always easier to deal with native types when writing business logic than 
constantly marshal and unmarshal stringly typed objects (looking at you JSON).

We shall accomplish this injection by generalized induction over the algebraic 
structure of RPL patterns. That is, we'll handle every possible way to make an 
RPL pattern from things that aren't patterns (such as character classes and 
string literals) as well as from subpatterns (like concatenation and ordered 
choice), simply recursing on subpatterns.

## Concatenation -> Struct
One of the easier recursive constructors to handle is that of concatenation. If 
we match some input against a concatenation of patterns, then we matched every 
component pattern and can instantiate a struct whose members correspond to the
subpatterns.

Rosie actually has two concatenation constructors, tokenized and raw. As far as
what types they represent, they're the same. The only difference is how the 
components would be "unparsed" into a text stream again. This can be handled 
easily enough by generating a stream insertion operator that inserts a token 
separator between components.

The one major sticking point is that RPL syntax doesn't have a way to label the 
components of a concatenation, while C++ needs each component of a struct to be 
named in order to access it. For the time being, we can simply use a templated 
accessor and an index number to directly grab the nth submatch. It's not ideal, 
but without adding a tagging syntax to RPL, there's not much that can be done 
directly. Since we usually want to produce something else from the matches we 
create, we can inject the tagging information there by only allowing access to 
the components via a function that takes all of them. The application logic can 
then name the parameters to that function whatever makes sense and generate 
whatever objects it needs.

## Ordered Choice -> Discriminated Union
While raw C style unions are problematic at best, when we combine them with a 
tag to indicate which member is active, we get a reasonable type that can be 
used safely. Rosie's greedy ordered choice makes this even easier, as we'll 
never match later instances of the same pattern, so each subpattern's type need
only show up once. 

We still have the same issue as Structs where we don't have any tagging 
information to name the various options, so we'll use indicies and 
transformation functions to do the same thing again. This instance, however, 
will require a collection of methods, one for each option. Since we can insist 
that each subpattern in the ordered choice be unique, resolving against a single
functor with an overloaded call operator is a suitable implementation. To 
directly construct an object, we can instead insist on suitable overloads of 
constructors.

## Repetition -> List
This is quite simple in theory, as we can simply create a std::list of suitable 
value type. The difference between `*` and `+` can by handled by writing 
separate wrappers where the base case constructor is empty or a single element,
respectively

## Option -> Optional


## String literal -> Type level constant
Generally string literals are used as tags in other expressions and thus aren't
part of the application data. However, there are cases where the string literal
is the distinguishing part between two options and thus it needs to distinctly 
exist at the type level to pick function overloads.

## Character class -> Char Subtype
This is 
