# Rosie Client cpp

C++ client for the Rosie Pattern matching engine and companion tools

## TODO
 - Formalize 'patterns are types' to define how to produce native C++ types from
   RPL patterns
 - Manually transform the RPL patterns for RPL into bootstrap types
 - Wrap the methods exposed by `librosie.h` into a "proper" C++ API
 - Write an automatic code generator using the bootstrap types
 - Generate the "self-hosted" RPL types and pivot the generator to use them
 - [long term] Create RPL patterns for parsing C++ into syntax trees suitable 
   for compilers
 - [long term] Use the "self-hosting" abilities to empower development of RPL 
   compiler extensions

## Patterns are Types
The basic idea stems from a thought in the RPL 1.0 syntax definition 
(`.../doc/rpl.md` Section "Matches/Captures")

>  The rationale for this decision [capturing every named pattern] is that the 
>  pattern name has the role of a type, and therefore can be used to tag a 
>  capture in a meaningful way. Another way to think about it is: if something 
>  is important enough that you want to see it captured in the output, then it 
>  deserves a name.

Asserting equivalence between named patterns in a pattern language and the 
concept of a type is a nice thing to say, but is it anything more than 
empty philosophizing to justify a design decision? It most certainly is. C++ 
isn't exactly known for an amazing type system, but it has enough to create a 
useful injection from RPL patterns to C++ types. Why make such a thing? It's 
always easier to deal with native types when writing business logic than 
constantly marshal and unmarshal stringly typed objects (looking at you JSON).
See `./design/patterns-are-types.md` for details. Doing this in C++ should be 
enough to show that other Rosie clients can and should provide similar 
facilities for their client languages.